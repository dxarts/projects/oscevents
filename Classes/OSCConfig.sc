OSCConfig {
	var <oscFuncs, <data;
	var <>addr, <presetData;
	classvar <configPath;

	*initClass {
		configPath = File.realpath(this.class.filenameSymbol).dirname.dirname +/+ 'config'
	}

	*new {
		^super.new.init
	}

	init {
		oscFuncs = IdentityDictionary.new(know: true);
		data = IdentityDictionary.new(know: true);
	}

	loadPreset { |preset, setFuncs = false|
		PathName(preset.asString).isAbsolutePath.not.if({
			preset =  configPath +/+ preset ++ ".scd";
		});

		File.exists(preset).if({
			File.use(preset, "r", { |f| presetData = f.readAllString.interpret });
			setFuncs.if({ this.setFuncs(presetData) })

		}, {
			("File not found at" + preset).warn;
		})
	}

	setFuncs { |id, replace = true|
		replace.if({ oscFuncs.do(_.free) });
		id.keysValuesDo{ |key, value|
			this.addUniqueMethod(key, { data[key] });
			oscFuncs[key] = OSCFunc({ |msg| data[key] = msg[1] }, value, addr)
		}
	}

	addFunc { |type, func, free = true|
		free.if({ this.freeFunc(type) });
		oscFuncs[type] = OSCFunc(func, presetData[type], addr)
	}

	freeFunc { |type| oscFuncs[type].free }

}