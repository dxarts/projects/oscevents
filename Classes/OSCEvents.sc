OSCEvent : DataEvent {

	*new { |time, val, path|
		^super.new(time, val, path)
	}

	path { ^this.id }

}

OSCTrack : DataTrack {

	*new { |path, events|
		^super.new(path, events)
	}

	path { ^this.id }
	convertPaths { this.convertIDs }

}

OSCEvents : DataEvents {
	var <>playTasks;

	*new { |tracks|
		^super.new(tracks, OSCTrack)
	}

	paths { ^this.tracks.collect(_.id) }

	play { |netAddr|
		netAddr ?? { netAddr = NetAddr.localAddr };
		playTasks = this.tracks.collect{ |oscTrack|
			Task({
				var now = 0.0;
				oscTrack.events.do{ |oscEvent|
					(oscEvent.time - now).wait;
					netAddr.sendMsg(oscEvent.path, oscEvent.val)
				}
			})
		};
		playTasks.play
	}

	stop { playTasks.do(_.stop) }

}

OSCRecorder {
	var <>path, <>netAddr;
	var <oscTrack, <>funcList, <>modFunc, <oscFuncs, <time;
	var <lastRecording, <recFunc, <startRec;

	*new { |path, netAddr|
		^super.newCopyArgs(path, netAddr).init
	}

	init {
		funcList = FunctionList.new;
		time = Clock.seconds;
		lastRecording = 0.0;
		path.isKindOf(Array).not.if({ path = [path] });
		oscFuncs = path.collect{ |thisPath|
			OSCFunc({ |msg|
				var oscEvent, val;
				val = msg[1..];
				(val.size < 2).if({ val = val[0] });
				oscEvent = OSCEvent.new(this.now, val, thisPath);
				modFunc.(oscEvent);
				funcList.(oscEvent)
			}, thisPath, netAddr)
		}

	}

	now {
		^Clock.seconds - time
	}

	learn {
		var oscLearn;
		oscFuncs.do(_.free);
		{
			oscLearn = OSCLearn.new.learn;
			while({ oscLearn.path.isNil }, { 0.01.wait });
			oscLearn.stop;
			this.path = oscLearn.path;
			this.netAddr = oscLearn.addr;
			this.init
		}.fork
	}

	record { |recordingStarttime = 0.0, relativeToLastRecording = true|
		var now = this.now;

		oscTrack.isNil.if({ oscTrack = OSCTrack.new(path) });

		relativeToLastRecording.if({ recordingStarttime = recordingStarttime + lastRecording });

		startRec = Clock.seconds;

		recFunc = { |oscEvent| oscTrack.add(oscEvent.time_(oscEvent.time + recordingStarttime - now)) };
		funcList.addFunc(recFunc);

	}

	resume { |recordingStarttime = 0.0|
		this.record(lastRecording, true)
	}

	stop {
		lastRecording = lastRecording + Clock.seconds - startRec;
		funcList.removeFunc(recFunc)
	}

	addFunc { |functions|
		funcList.addFunc(functions)
	}

	removeFunc { |functions|
		funcList.removeFunc(functions)
	}

	oscEvents {
		^OSCEvents.new([oscTrack])
	}

	cleanup {
		funcList = FunctionList.new;
		oscFuncs.do(_.free);
	}

}