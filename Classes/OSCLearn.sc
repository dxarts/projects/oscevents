OSCLearn {
	var <>path, <addr, <recvPort;
	var learnFunc, <oscFunc, <>msg;
	var <isLearning = false;

	*new {
		^super.new.init
	}

	init {
		learnFunc = { |thismsg, time, thisaddr, thisRecvPort|
			(Server.all.any{ |it| it.addr == thisaddr }).not.if({
				this.msg_(thismsg);
				this.path_(msg[0]);
				this.addr_(thisaddr)
			})
		}
	}

	learn {
		// protect from learning twice
		isLearning.if({ this.stop });
		thisProcess.addOSCRecvFunc(learnFunc);
		isLearning = true
	}

	stop {
		thisProcess.removeOSCRecvFunc(learnFunc);
		isLearning = false
	}

	connectTo { |func|
		this.disconnect;
		oscFunc = OSCFunc.new(func, path, addr)
	}

	disconnect { oscFunc !? { oscFunc.free } }

	reset {
		isLearning.if({ this.stop });
		this.disconnect;
		addr = nil;
		path = nil;
		recvPort = nil
	}

	addr_ { |aNetAddr| addr = aNetAddr; recvPort = aNetAddr.port }

	num { ^addr }
	type { ^path }

}

